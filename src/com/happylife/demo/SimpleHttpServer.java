package com.happylife.demo;

import java.io.File;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.happylife.demo.servlet.http.HttpExchangeRequestAdaptor;
import com.happylife.demo.servlet.http.HttpExchangeResponseAdaptor;
import com.happylife.demo.servlet.http.HttpRequest;
import com.happylife.demo.servlet.http.HttpResponse;
import com.happylife.demo.servlet.http.SimpleServlet;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

public class SimpleHttpServer {
    private static final String ROOT_PACKAGE_NAME = "com.happylife.demo";
    
	private HttpServer server;
	private Map<String, SimpleServlet> servletInstances =
	        new HashMap<String, SimpleServlet>();
	
	/**
	 * HTTP 요청을 받아서 SimpleServlet의 service 메소드를 실행시켜주는 객체.
	 */
	private HttpHandler servletExecutor = new HttpHandler() {
        
        @Override
        public void handle(HttpExchange exchange) throws IOException {
            String path = exchange.getRequestURI().getPath();
            HttpRequest req = new HttpExchangeRequestAdaptor(exchange);
            HttpResponse res = new HttpExchangeResponseAdaptor(exchange);
            
            servletInstances.get(path).service(req, res);
        }
    };
    
    private static List<Class<?>> getClasses(String packageName) throws ClassNotFoundException, IOException {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        assert classLoader != null;
        
        String path = packageName.replace('.', '/');
        Enumeration<URL> resources = classLoader.getResources(path);
        List<File> dirs = new ArrayList<File>();
        while (resources.hasMoreElements()) {
            URL resource = resources.nextElement();
            dirs.add(new File(resource.getFile()));
        }
        
        List<Class<?>> classes = new ArrayList<Class<?>>();
        for (File directory : dirs) {
            classes.addAll(findClasses(directory, packageName));
        }
        return classes;
    }
    
    private static List<Class<?>> findClasses(File directory, String packageName) throws ClassNotFoundException {
        List<Class<?>> classes = new ArrayList<Class<?>>();
        
        if (!directory.exists()) {
            return classes;
        }
        File[] files = directory.listFiles();
        for (File file : files) {
            if (file.isDirectory()) {
                assert !file.getName().contains(".");
                classes.addAll(findClasses(file, packageName + "." + file.getName()));
            } else if (file.getName().endsWith(".class")) {
                classes.add(Class.forName(packageName + '.' + file.getName().substring(0, file.getName().length() - 6)));
            }
        }
        return classes;
    }
    
    /**
     * SimpleServlet을 구현한 클래스를 인스턴스화 시켜서 List 형태로 리턴.
     * 
     * @return
     * @throws ClassNotFoundException
     * @throws IOException
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    private List<SimpleServlet> scanServlets() 
            throws ClassNotFoundException, IOException, InstantiationException, 
                   IllegalAccessException {
        List<SimpleServlet> servletInstances = new ArrayList<SimpleServlet>();
        List<Class<?>> classes = getClasses(ROOT_PACKAGE_NAME);
        
        if (classes == null || classes.isEmpty()) {
            System.err.println("Couldn't find any classes under the " +
                ROOT_PACKAGE_NAME + " package.");
            return servletInstances;
        }
        
        for (Class<?> clazz: classes) {
            if (clazz.isInterface() == false && SimpleServlet.class.isAssignableFrom(clazz))
                servletInstances.add((SimpleServlet) clazz.newInstance());
        }
        return servletInstances;
    }

	public void Start(int port) {
		try {
		    List<SimpleServlet> servletClassInstances;
		    
			server = HttpServer.create(new InetSocketAddress(port), 0);
			servletClassInstances = scanServlets();
			for (SimpleServlet servlet: servletClassInstances) {
			    String pkgName = servlet.getClass().getName();
			    
			    System.out.println(String.format("Servlet class %s instantiated", 
			        pkgName)
			    );
			    pkgName = pkgName.replace(ROOT_PACKAGE_NAME, "");
			    pkgName = pkgName.replaceAll("\\.", "/");
			
			    System.out.println("URI Path: " + pkgName);
			    
			    servletInstances.put(pkgName, servlet);
			    /*
			     * HttpServer.createContext 메소드는 Handler 객체로 HttpHandler 
			     * 타입밖에 받을 수 없기 때문에, 우선 servletExecutor를 넘겨서 요청
			     * 을 처리하도록 한다.
			     */
			    server.createContext(pkgName, servletExecutor);
			}
			
			System.out.println("server started at " + port);
			server.setExecutor(null);
			server.start();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
	}

	public void Stop() {
		server.stop(0);
		System.out.println("server stopped");
	}
}
