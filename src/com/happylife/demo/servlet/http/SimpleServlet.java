package com.happylife.demo.servlet.http;

public interface SimpleServlet {
    void service(HttpRequest req, HttpResponse res);
}
